---
date: '2020-04-01'
title: 'Creative Designer'
company: 'Danfe Creatives'
location: 'Anamnagar, Kathmandu'
range: 'April - December 2020'
url: 'https://creativesdanfe.com/'
---

- Developed and maintained websites for in-house and client websites primarily using major as Wordpress, Woo-Commerce, Ghost, HTML, CSS, JavaScript and more
- Mastered the differents plugins and builders used for CMS like Wordpress, Woocommerce and Ghost creating outstanding web pages and templetes form scratch
- Manually tested sites in various browsers and mobile devices to ensure cross-browser compatibility and responsiveness
- Clients included [Modern Decks](http://moderndecks.great-site.net/) an International Company, [The MoonSun Group](https://themoonsungroup.com/) an Investment Company, [GS Bazzar](https://gsonlinebazar.com/) a Multi-Vendor, [Nepal Art Gallary](http://nepalartgallery.great-site.net/) a Project Showcase for artist and more..
- Interfaced with clients on a weekly basis, providing technological expertise
