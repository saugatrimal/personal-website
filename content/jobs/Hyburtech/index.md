---
date: '2021-06-01'
title: 'WordPress Developer'
company: 'Hyburtech'
location: 'Mumbai, India'
range: 'May 2021 - Present'
url: 'https://hyburtech.com/'
---

- Work with a variety of different languages, platforms, frameworks, and content management systems such as JavaScript, TypeScript, Gatsby, React, Craft, WordPress, Prismic, and Netlify
- Worked with a team of four designers & developers to build a blog websites, marketing website and e-commerce platform, an ambitious startup
- Interfaced with user experience designers and other developers to ensure thoughtful and coherent user experiences
- Contributed on making the sites developed more SEO friendly along with other SEO experts
- Increased the performance and speed of web pages which helps ranking the pages in top
- Communicate with multi-disciplinary teams of engineers, designers, producers, and clients on a daily basis


