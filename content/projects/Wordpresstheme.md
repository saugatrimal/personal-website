---
date: '2021-07-25'
title: 'Made a Custom Wordpress Theme'
github: 'https://github.com/saugat-rimal'
external: 'https://medium.com/stories-from-upstatement/integrating-algolia-search-with-wordpress-multisite-e2dea3ed449c'
tech:
  - Html
  - Css3
  - WordPress
  - PHP
company: 'Personal Project'
showInProjects: true
---

Building a custom multisite compatible wordPress theme to build awesome projects.
